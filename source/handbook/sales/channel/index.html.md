---
layout: handbook-page-toc
title: "Channel"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Channel Handbook

The Channel is a critical part of our strategy moving forward as it will help us 1) drive growth ARR through services capacity and capability to drive customer adoption and usage of the Gitlab platform and 2) drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.

### Channel KR

[OKRs](/company/okrs/)

### Channel Value 

The Channel is a critical part of our strategy moving forward as it will help us 1) drive growth ARR through services capacity and capability to drive customer adoption and usage of the Gitlab platform and 2) drive and increase new customer ARR through their relationships, service engagements, and knowledge of accounts.



## Channel Partner Types 

### Alliances

[Alliances](/handbook/alliances/)

### Resellers 

Primary monetization is through reselling GitLab licenses and services. Resellers can be a service partner too (often known as a Solution Provider).

1. VAR/VAD (Value Added Reseller or Distributor) - Channel services including resale, implementation, contracting, support, financing etc.
2. DMR (Direct Market Reseller)- primary business is resale of the software, often does not implement. Value are the contracts that these partners have in place with customers.

[Learn how to become a Gitlab reseller today](/handbook/resellers/)

### Services partners. 

Primary monetization is through the sale of services. This can be a one-time implementation, ongoing support or advisory, managed services or outsourcing. Services partners will resell Gitlab services, deliver services on behalf of Gitlab or deliver Gitlab certified services.  Services partners can be a reseller partner too (often known as a Solution Provider).

1. Global Systems Integrators - have a large global workforce and can deliver on almost any customer need. Examples: Accenture, Deloitte, TCS, Wipro
2. Regional Systems Integrators - large workforce but with single continental focus and a more limited offering of services. Examples: CI&T, Slalom
3. Boutique Systems Integrators - very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it. Examples - CloudReach, Flux7
4. Managed Service Providers - provide ongoing support for solutions/applications. Examples: Rackspace

### Existing Gitlab Partner Program

[Resellers program](/resellers/program/)

### Gitlab Channel Program Updates - March 2020

1. Building a channel of enabled, DevOps & Digital Transformation focused resellers and services providers. 
2. Provide eStore access for SMB & Midmarket channel partners 
3. Net neutral to Gitlab seller compensation 
4. Incentives to identify net new customers & opportunities in existing customers.
5. Incentives to attach product, operational & strategic services.
6. Referral fees for non reselling services partners.
7. MDF available for Demand Generation activities and events
8. Sales & SE Enablement available on demand;  certifications - H2’FY21 
9. Services Certifications - H2’FY21
10. Renewals incumbent protection: If a partner sells a deal and is in good standing (actively supporting the customer, etc) that partner receives first right of refusal for renewal; unless otherwise stated by the customer.

[Internal - Channel Strategy & Program Updates FY21](https://gitlab.com/gitlab-com/sales-team/field-operations/channel/-/wikis/Channel-home)